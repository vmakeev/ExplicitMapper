﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace ExplicitMapper.MappingGenerators
{
	internal interface IReferencedMethodInfo
	{
		string Name { get; }

		IList<ITypeSymbol> ParameterTypes { get; }

		ITypeSymbol ResultType { get; }
	}
}