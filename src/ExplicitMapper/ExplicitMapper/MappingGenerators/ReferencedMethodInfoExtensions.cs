﻿namespace ExplicitMapper.MappingGenerators
{
	internal static class ReferencedMethodInfoExtensions
	{
		public static bool IsSpecialMapCollectionMethod(this IReferencedMethodInfo methodInfo)
		{
			return methodInfo.Name == "MapCollection";
		}
	}
}