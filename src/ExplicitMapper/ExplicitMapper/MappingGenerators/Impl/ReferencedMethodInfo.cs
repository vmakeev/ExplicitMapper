﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace ExplicitMapper.MappingGenerators.Impl
{
	internal class ReferencedMethodInfo : IReferencedMethodInfo
	{
		public ReferencedMethodInfo(string name, ITypeSymbol resultType, IEnumerable<ITypeSymbol> parameterTypes)
		{
			Name = name;
			ParameterTypes = parameterTypes?.ToList() ?? new List<ITypeSymbol>();
			ResultType = resultType;
		}

		public string Name { get; }

		public IList<ITypeSymbol> ParameterTypes { get; }

		public ITypeSymbol ResultType { get; }
	}
}