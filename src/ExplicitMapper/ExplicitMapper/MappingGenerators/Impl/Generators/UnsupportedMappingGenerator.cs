﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExplicitMapper.Extensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace ExplicitMapper.MappingGenerators.Impl.Generators
{
	internal class UnsupportedMappingGenerator : BaseMappingGenerator
	{
		public override bool IsActuallySupported { get; } = false;

		public virtual string GetErrorMessage() => "todo: unsupported mapping type, implement manually";

		/// <inheritdoc />
		public override Task<(BlockSyntax blockSyntax, IEnumerable<IReferencedMethodInfo> referencedMethods)> GenerateMappingMethodBody(ParameterSyntax sourceSyntax,
			TypeSyntax targetSyntax,
			SyntaxToken targetIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull,
			CancellationToken cancellationToken)
		{
			var statements = new LinkedList<StatementSyntax>();

			// source object null check
			if (sourceType.CanBeNull() && allowParameterNull)
			{
				ExpressionSyntax condition = BinaryExpression(
					kind: SyntaxKind.EqualsExpression,
					left: IdentifierName(sourceSyntax.Identifier),
					right: LiteralExpression(SyntaxKind.NullLiteralExpression));

				StatementSyntax ifSourceIsNullStatement = targetType.CanBeNull()
					? ReturnStatement(LiteralExpression(SyntaxKind.NullLiteralExpression))
					: ReturnStatement(IdentifierName("CAN_NOT_RETURN_NULL"));

				BlockSyntax ifStatementBlock = Block(ifSourceIsNullStatement);
				IfStatementSyntax ifNullStatement = IfStatement(
						condition: condition,
						statement: ifStatementBlock)
					.WithTrailingTrivia(CarriageReturnLineFeed, CarriageReturnLineFeed);

				statements.AddLast(ifNullStatement);
			}

			IdentifierNameSyntax body = IdentifierName("CAN_NOT_GENERATE_MAPPING")
				.WithLeadingComment(GetErrorMessage());

			statements.AddLast(ExpressionStatement(body));

			BlockSyntax result = Block(statements);

			return Task.FromResult((result, Enumerable.Empty<IReferencedMethodInfo>()));
		}
	}
}