﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExplicitMapper.Extensions;
using ExplicitMapper.Matchers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace ExplicitMapper.MappingGenerators.Impl.Generators
{
	internal class EnumMappingGenerator : BaseMappingGenerator
	{
		public override bool IsActuallySupported { get; } = true;

		/// <inheritdoc />
		public override Task<(BlockSyntax blockSyntax, IEnumerable<IReferencedMethodInfo> referencedMethods)> GenerateMappingMethodBody(ParameterSyntax sourceSyntax,
			TypeSyntax targetSyntax,
			SyntaxToken targetIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull,
			CancellationToken cancellationToken)
		{
			var statements = new List<StatementSyntax>();

			if (!allowParameterNull && sourceType.CanBeNull())
			{
				ExpressionSyntax condition = BinaryExpression(
					kind: SyntaxKind.EqualsExpression,
					left: IdentifierName(sourceSyntax.Identifier),
					right: LiteralExpression(SyntaxKind.NullLiteralExpression));

				StatementSyntax ifSourceIsNullStatement = ParseStatement("throw new NullReferenceException($\"{nameof(" + sourceSyntax.Identifier.Text + ")} can not be null\");");

				BlockSyntax ifStatementBlock = Block(ifSourceIsNullStatement);
				IfStatementSyntax ifNullStatement = IfStatement(
						condition: condition,
						statement: ifStatementBlock)
					.WithTrailingTrivia(CarriageReturnLineFeed, CarriageReturnLineFeed);

				statements.Add(ifNullStatement);
			}

			SyntaxList<SwitchSectionSyntax> switchStatements = GenerateStatements(sourceSyntax.Identifier, sourceType, targetType, allowParameterNull);
			SwitchStatementSyntax switchStatement = SwitchStatement(IdentifierName(sourceSyntax.Identifier), switchStatements);

			statements.Add(switchStatement);

			return Task.FromResult((Block(statements), Enumerable.Empty<IReferencedMethodInfo>()));
		}

		private SyntaxList<SwitchSectionSyntax> GenerateStatements(SyntaxToken sourceIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull)
		{
			var result = new List<SwitchSectionSyntax>();

			IEnumerable<Match<IFieldSymbol>> membersMap = EnumMembersMatcher.MatchMembers(sourceType, targetType);

			ITypeSymbol notNullableSourceType = sourceType.UnwrapIfNullable();
			ITypeSymbol notNullableTargetType = targetType.UnwrapIfNullable();

			// map null section
			if (sourceType.CanBeNull() && allowParameterNull)
			{
				SyntaxList<SwitchLabelSyntax> caseLabels = List<SwitchLabelSyntax>(new[]
				{
					CaseSwitchLabel(LiteralExpression(SyntaxKind.NullLiteralExpression))
						.WithLeadingTrivia(CarriageReturnLineFeed)
				});

				SyntaxList<StatementSyntax> statements = List<StatementSyntax>(new[]
				{
					targetType.CanBeNull()
						? ReturnStatement(LiteralExpression(SyntaxKind.NullLiteralExpression))
							.WithTrailingTrivia(CarriageReturnLineFeed)
						: ReturnStatement(IdentifierName("CAN_NOT_MAP_NULL"))
							.WithTrailingTrivia(CarriageReturnLineFeed)
				});

				result.Add(SwitchSection(caseLabels, statements));
			}

			// map values
			foreach ((IFieldSymbol from, IFieldSymbol to, MatchType matchType) in membersMap)
			{
				SyntaxList<SwitchLabelSyntax> caseLabels = List<SwitchLabelSyntax>(new[]
				{
					CaseSwitchLabel(GetMemberAccess(notNullableSourceType.Name, from.Name))
						.WithLeadingTrivia(CarriageReturnLineFeed)
				});

				SyntaxList<StatementSyntax> statements = List<StatementSyntax>(new[]
				{
					ReturnStatement(GetMemberAccess(notNullableTargetType.Name, matchType != MatchType.NotMatch ? to.Name : "NO_MATCHING_ENUM_MEMBER_FOUND!"))
						.WithTrailingTrivia(CarriageReturnLineFeed)
						.WithLeadingCommentWhen(() => matchType == MatchType.PossibleMatch, "todo: verify next line mapping!")
						.WithLeadingCommentWhen(() => matchType == MatchType.NotMatch, "todo: fix next line mapping")
				});

				result.Add(SwitchSection(caseLabels, statements));
			}

			// throw section
			{
				SyntaxList<SwitchLabelSyntax> caseLabels = List<SwitchLabelSyntax>(new[]
				{
					DefaultSwitchLabel()
						.WithLeadingTrivia(CarriageReturnLineFeed)
				});

				SyntaxList<StatementSyntax> statements = List(new[]
				{
					GetThrowStatement(sourceIdentifier)
				});

				result.Add(SwitchSection(caseLabels, statements));
			}

			return List(result);
		}

		private StatementSyntax GetThrowStatement(SyntaxToken sourceIdentifier)
		{
			// todo: remove parsing
			string throwStatement =
				@"throw new ArgumentOutOfRangeException($""Unexpected {$ID$.GetType().Name}: {$ID$:G} ({$ID$:D}"", (Exception)null);".Replace("$ID$", sourceIdentifier.Text);
			return ParseStatement(throwStatement);
		}
	}
}