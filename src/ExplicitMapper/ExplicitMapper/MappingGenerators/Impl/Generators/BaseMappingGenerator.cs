﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace ExplicitMapper.MappingGenerators.Impl.Generators
{
	internal abstract class BaseMappingGenerator : IMappingGenerator
	{
		protected string MapCollectionMethodName { get; } = "MapCollection";

		public abstract bool IsActuallySupported { get; }

		/// <inheritdoc />
		public abstract Task<(BlockSyntax blockSyntax, IEnumerable<IReferencedMethodInfo> referencedMethods)> GenerateMappingMethodBody(ParameterSyntax sourceSyntax,
			TypeSyntax targetSyntax,
			SyntaxToken targetIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull,
			CancellationToken cancellationToken);

		protected MemberAccessExpressionSyntax GetMemberAccess(SyntaxToken target, string memberName)
		{
			MemberAccessExpressionSyntax memberAccessExpression = MemberAccessExpression(
				kind: SyntaxKind.SimpleMemberAccessExpression,
				expression: IdentifierName(target),
				name: IdentifierName(memberName));

			return memberAccessExpression;
		}

		protected MemberAccessExpressionSyntax GetMemberAccess(string target, string memberName)
		{
			MemberAccessExpressionSyntax memberAccessExpression = MemberAccessExpression(
				kind: SyntaxKind.SimpleMemberAccessExpression,
				expression: IdentifierName(target),
				name: IdentifierName(memberName));

			return memberAccessExpression;
		}
	}
}