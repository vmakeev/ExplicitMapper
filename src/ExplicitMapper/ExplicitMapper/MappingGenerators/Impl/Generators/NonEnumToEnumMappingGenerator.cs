﻿namespace ExplicitMapper.MappingGenerators.Impl.Generators
{
	internal class NonEnumToEnumMappingGenerator : UnsupportedMappingGenerator
	{
		public override string GetErrorMessage() => "todo: unsupported mapping type (non-enum to enum), implement manually";
	}
}