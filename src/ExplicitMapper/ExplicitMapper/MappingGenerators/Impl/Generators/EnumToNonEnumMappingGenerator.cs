﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExplicitMapper.Extensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace ExplicitMapper.MappingGenerators.Impl.Generators
{
	internal class EnumToNonEnumMappingGenerator : BaseMappingGenerator
	{
		public override bool IsActuallySupported { get; } = false;

		/// <inheritdoc />
		public override Task<(BlockSyntax blockSyntax, IEnumerable<IReferencedMethodInfo> referencedMethods)> GenerateMappingMethodBody(ParameterSyntax sourceSyntax,
			TypeSyntax targetSyntax,
			SyntaxToken targetIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull,
			CancellationToken cancellationToken)
		{
			var statements = new List<StatementSyntax>();

			if (sourceType.CanBeNull())
			{
				ExpressionSyntax condition = BinaryExpression(
					kind: SyntaxKind.EqualsExpression,
					left: IdentifierName(sourceSyntax.Identifier),
					right: LiteralExpression(SyntaxKind.NullLiteralExpression));

				StatementSyntax ifSourceIsNullStatement = ParseStatement("throw new NullReferenceException($\"{nameof(" + sourceSyntax.Identifier.Text + ")} can not be null\");");

				BlockSyntax ifStatementBlock = Block(ifSourceIsNullStatement);
				IfStatementSyntax ifNullStatement = IfStatement(
						condition: condition,
						statement: ifStatementBlock)
					.WithTrailingTrivia(CarriageReturnLineFeed, CarriageReturnLineFeed);

				statements.Add(ifNullStatement);
			}

			SyntaxList<SwitchSectionSyntax> switchStatements = GenerateStatements(sourceSyntax.Identifier, sourceType, targetType, allowParameterNull);

			SwitchStatementSyntax switchStatement = SwitchStatement(IdentifierName(sourceSyntax.Identifier), switchStatements)
																.WithLeadingComment("todo: unsupported mapping type (enum to non-enum), implement manually");

			statements.Add(switchStatement);

			return Task.FromResult((Block(statements), Enumerable.Empty<IReferencedMethodInfo>()));
		}

		private SyntaxList<SwitchSectionSyntax> GenerateStatements(SyntaxToken sourceIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
		bool allowParameterNull)
		{
			var result = new List<SwitchSectionSyntax>();

			ITypeSymbol notNullableSourceType = sourceType.UnwrapIfNullable();

			IEnumerable<IFieldSymbol> sourceMembers = sourceType.GetEnumMembers();

			// map null section
			if (sourceType.CanBeNull() && allowParameterNull)
			{
				SyntaxList<SwitchLabelSyntax> caseLabels = List<SwitchLabelSyntax>(new[]
				{
					CaseSwitchLabel(LiteralExpression(SyntaxKind.NullLiteralExpression))
								.WithLeadingTrivia(CarriageReturnLineFeed)
				});

				SyntaxList<StatementSyntax> statements = List<StatementSyntax>(new[]
				{
					targetType.CanBeNull()
						? ReturnStatement(LiteralExpression(SyntaxKind.NullLiteralExpression))
										.WithTrailingTrivia(CarriageReturnLineFeed)
						: ReturnStatement(IdentifierName("CAN_NOT_MAP_NULL"))
										.WithTrailingTrivia(CarriageReturnLineFeed)
				});

				result.Add(SwitchSection(caseLabels, statements));
			}

			// map values
			foreach (IFieldSymbol enumMember in sourceMembers)
			{
				SyntaxList<SwitchLabelSyntax> caseLabels = List<SwitchLabelSyntax>(new[]
				{
					CaseSwitchLabel(GetMemberAccess(notNullableSourceType.Name, enumMember.Name))
								.WithLeadingTrivia(CarriageReturnLineFeed)
				});

				SyntaxList<StatementSyntax> statements = List<StatementSyntax>(new[]
				{
					ReturnStatement(IdentifierName("CAN_NOT_GENERATE_MAPPING"))
								.WithTrailingTrivia(CarriageReturnLineFeed)
				});

				result.Add(SwitchSection(caseLabels, statements));
			}

			// throw section
			{
				SyntaxList<SwitchLabelSyntax> caseLabels = List<SwitchLabelSyntax>(new[]
				{
					DefaultSwitchLabel()
								.WithLeadingTrivia(CarriageReturnLineFeed)
				});

				SyntaxList<StatementSyntax> statements = List(new[]
				{
					GetThrowStatement(sourceIdentifier)
				});

				result.Add(SwitchSection(caseLabels, statements));
			}

			return List(result);
		}

		private StatementSyntax GetThrowStatement(SyntaxToken sourceIdentifier)
		{
			// todo: remove parsing
			string throwStatement =
				@"throw new ArgumentOutOfRangeException($""Unexpected {$ID$.GetType().Name}: {$ID$:G} ({$ID$:D}"", (Exception)null);".Replace("$ID$", sourceIdentifier.Text);
			return ParseStatement(throwStatement);
		}
	}
}