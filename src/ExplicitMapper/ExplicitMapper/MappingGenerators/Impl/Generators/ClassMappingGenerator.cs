﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExplicitMapper.Extensions;
using ExplicitMapper.Matchers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace ExplicitMapper.MappingGenerators.Impl.Generators
{
	internal class ClassMappingGenerator : BaseMappingGenerator
	{
		public override bool IsActuallySupported { get; } = true;

		/// <inheritdoc />
		public override async Task<(BlockSyntax blockSyntax, IEnumerable<IReferencedMethodInfo> referencedMethods)> GenerateMappingMethodBody(ParameterSyntax sourceSyntax,
			TypeSyntax targetSyntax,
			SyntaxToken targetIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull,
			CancellationToken cancellationToken)
		{
			var statements = new LinkedList<StatementSyntax>();

			// source object null check
			if (sourceType.CanBeNull() && allowParameterNull)
			{
				ExpressionSyntax condition = BinaryExpression(
					kind: SyntaxKind.EqualsExpression,
					left: IdentifierName(sourceSyntax.Identifier),
					right: LiteralExpression(SyntaxKind.NullLiteralExpression));

				StatementSyntax ifSourceIsNullStatement = targetType.CanBeNull()
					? ReturnStatement(LiteralExpression(SyntaxKind.NullLiteralExpression))
					: ReturnStatement(IdentifierName("CAN_NOT_RETURN_NULL"));

				BlockSyntax ifStatementBlock = Block(ifSourceIsNullStatement);
				IfStatementSyntax ifNullStatement = IfStatement(
						condition: condition,
						statement: ifStatementBlock)
					.WithTrailingTrivia(CarriageReturnLineFeed, CarriageReturnLineFeed);

				statements.AddLast(ifNullStatement);
			}

			// target object creation

			IEnumerable<Match<IPropertySymbol>> propertiesMap = TypePropertiesMatcher.MatchProperties(sourceType, targetType).ToArray();

			ICollection<ExpressionSyntax> initializerMembers = await GetMapInitializersAsync(sourceSyntax, propertiesMap, cancellationToken).ConfigureAwait(false);
			// `delimiters.Count` should be `initializerMembers.Count-1`
			IEnumerable<SyntaxToken> delimiters =
				initializerMembers.Skip(1).Select(_ => Token(SyntaxTriviaList.Empty, SyntaxKind.CommaToken, SyntaxTriviaList.Create(CarriageReturnLineFeed)));
			// object initializer via { ... }
			InitializerExpressionSyntax initializer = InitializerExpression(SyntaxKind.ObjectInitializerExpression, SeparatedList(initializerMembers, delimiters));
			// object creation
			ObjectCreationExpressionSyntax targetCreationExpression = ObjectCreationExpression(targetSyntax, ArgumentList(), initializer);

			VariableDeclaratorSyntax variableDeclaration = VariableDeclarator(
				identifier: targetIdentifier,
				argumentList: null,
				initializer: EqualsValueClause(targetCreationExpression));

			LocalDeclarationStatementSyntax targetDeclaration = LocalDeclarationStatement(
					declaration: VariableDeclaration(
						type: IdentifierName("var"), // todo: use token
						variables: SeparatedList(new[] { variableDeclaration })))
				.WithTrailingTrivia(CarriageReturnLineFeed);
			statements.AddLast(targetDeclaration);

			// return initialized target

			ReturnStatementSyntax returnStatement = ReturnStatement(IdentifierName(targetIdentifier)).WithLeadingTrivia(CarriageReturnLineFeed);
			statements.AddLast(returnStatement);

			BlockSyntax result = Block(statements);

			var references = new List<IReferencedMethodInfo>();

			foreach (AssignmentExpressionSyntax member in initializerMembers.OfType<AssignmentExpressionSyntax>())
			{
				Match<IPropertySymbol> match = null;

				if (member.Left is IdentifierNameSyntax targetMemberIdNameSyntax)
				{
					string targetMemberName = targetMemberIdNameSyntax.Identifier.Text;
					match = propertiesMap.SingleOrDefault(item => item.Target.Name == targetMemberName);
				}

				if (match == null)
				{
					continue;
				}

				IPropertySymbol sourceItemType = match.Source;
				IPropertySymbol targetItemType = match.Target;

				if (member.Right is InvocationExpressionSyntax iex)
				{
					if (iex.Expression is IdentifierNameSyntax methodIdentfier)
					{
						string methodName = methodIdentfier.Identifier.Text;

						if (methodName != MapCollectionMethodName)
						{
							var info = new ReferencedMethodInfo(methodName, targetItemType.Type, new[] { sourceItemType.Type });
							references.Add(info);
						}
						else
						{
							var mapCollectionItself = new ReferencedMethodInfo(methodName, null, null);
							references.Add(mapCollectionItself);

							if (iex.ArgumentList?.Arguments.Count >= 2 && iex.ArgumentList.Arguments[1].Expression is IdentifierNameSyntax methodNameSyntax)
							{
								ITypeSymbol methodSourceType = sourceItemType.Type.GetUnderlyingType();

								ITypeSymbol methodTargetType = targetItemType.Type.IsIEnumerable(true)
									? targetItemType.Type.GetUnderlyingType()
									: targetItemType.Type;

								var info = new ReferencedMethodInfo(methodNameSyntax.Identifier.Text, methodTargetType, new[] { methodSourceType });
								references.Add(info);
							}
						}
					}
				}
			}

			return (result, references);
		}

		private Task<ICollection<ExpressionSyntax>> GetMapInitializersAsync(ParameterSyntax sourceSyntax,
			IEnumerable<Match<IPropertySymbol>> propertiesMap,
			CancellationToken cancellationToken)
		{
			ICollection<ExpressionSyntax> result = new List<ExpressionSyntax>();
			foreach ((IPropertySymbol sourceProperty, IPropertySymbol targetProperty, MatchType matchType) in propertiesMap)
			{
				cancellationToken.ThrowIfCancellationRequested();

				// missing property in source object
				if (sourceProperty == null || matchType == MatchType.NotMatch)
				{
					result.Add(GetMissingMemberAssignment(targetProperty, matchType));
				}
				// source property is ICollection<T>
				else if (sourceProperty.Type.IsICollection())
				{
					result.Add(
						targetProperty.Type.IsIEnumerable(true)
							// target object's property is enumerable
							? GetMapCollectionAssignment(sourceSyntax, sourceProperty, targetProperty, matchType)
							// target object' s property is not an enumerable
							: GetUnsupportedMappingTypeAssignment(sourceSyntax, sourceProperty, targetProperty, matchType));
				}
				// source property is IEnumerable<T>
				else if (sourceProperty.Type.IsIEnumerable(true))
				{
					result.Add(
						targetProperty.Type.IsIEnumerable(true)
							// target object's property is enumerable
							? GetMapCollectionWithEnumerationAssignment(sourceSyntax, sourceProperty, targetProperty, matchType)
							// target object' s property is not an enumerable
							: GetUnsupportedMappingTypeAssignment(sourceSyntax, sourceProperty, targetProperty, matchType));
				}
				// source type is compatible with target type
				else if (sourceProperty.Type.CanBeDirectlyMappedTo(targetProperty.Type))
				{
					result.Add(GetDirectAssignment(sourceSyntax, sourceProperty, targetProperty, matchType));
				}
				else
				{
					result.Add(GetMappingAssignment(sourceSyntax, sourceProperty, targetProperty, matchType));
				}
			}

			return Task.FromResult(result);
		}

		[SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
		private ExpressionSyntax GetMapCollectionWithEnumerationAssignment(ParameterSyntax sourceSyntax,
			IPropertySymbol sourceProperty,
			IPropertySymbol targetProperty,
			MatchType matchType)
		{
			MemberAccessExpressionSyntax memberAccessExpression = GetMemberAccess(sourceSyntax.Identifier, sourceProperty.Name);

			InvocationExpressionSyntax toListInvocationExpression = InvocationExpression(MemberBindingExpression(IdentifierName("ToList")));
			ConditionalAccessExpressionSyntax enumeratedValueExpression = ConditionalAccessExpression(memberAccessExpression, toListInvocationExpression);

			ExpressionSyntax methodIdentifier = IdentifierName(MapCollectionMethodName);
			ArgumentSyntax argument = Argument(enumeratedValueExpression);
			ArgumentSyntax mapMethod = GetMapCollectionMapMethod(sourceProperty, targetProperty);

			InvocationExpressionSyntax invocationExpression = InvocationExpression(methodIdentifier, ArgumentList(SeparatedList(new[] { argument, mapMethod })));

			return AssignmentExpression(SyntaxKind.SimpleAssignmentExpression, IdentifierName(targetProperty.Name), invocationExpression)
				.WithLeadingCommentWhen(() => matchType != MatchType.Match, "todo: verify next line mapping!");
		}

		private ArgumentSyntax GetMapCollectionMapMethod(IPropertySymbol sourceProperty, IPropertySymbol targetProperty)
		{
			ITypeSymbol sourceType = sourceProperty.Type.GetUnderlyingType();
			ITypeSymbol targetType = targetProperty.Type.GetUnderlyingType();

			if (sourceType.CanBeDirectlyMappedTo(targetType))
			{
				ExpressionSyntax lambdaSyntax = SimpleLambdaExpression(Parameter(Identifier("_")), IdentifierName("_"));
				return Argument(lambdaSyntax);
			}

			return Argument(IdentifierName($"To{targetProperty.Type.GetUnderlyingSemanticName()}"));
		}

		[SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
		private ExpressionSyntax GetMapCollectionAssignment(ParameterSyntax sourceSyntax, IPropertySymbol sourceProperty, IPropertySymbol targetProperty, MatchType matchType)
		{
			MemberAccessExpressionSyntax memberAccessExpression = GetMemberAccess(sourceSyntax.Identifier, sourceProperty.Name);

			ExpressionSyntax methodIdentifier = IdentifierName(MapCollectionMethodName);
			ArgumentSyntax argument = Argument(memberAccessExpression);
			ArgumentSyntax mapMethod = GetMapCollectionMapMethod(sourceProperty, targetProperty);

			InvocationExpressionSyntax invocationExpression = InvocationExpression(methodIdentifier, ArgumentList(SeparatedList(new[] { argument, mapMethod })));

			return AssignmentExpression(SyntaxKind.SimpleAssignmentExpression, IdentifierName(targetProperty.Name), invocationExpression)
				.WithLeadingCommentWhen(() => matchType != MatchType.Match, "todo: verify next line mapping!");
		}

		[SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
		private ExpressionSyntax GetMappingAssignment(ParameterSyntax sourceSyntax, IPropertySymbol sourceProperty, IPropertySymbol targetProperty, MatchType matchType)
		{
			MemberAccessExpressionSyntax memberAccessExpression = GetMemberAccess(sourceSyntax.Identifier, sourceProperty.Name);

			ExpressionSyntax methodIdentifier = IdentifierName($"To{targetProperty.Type.GetSemanticName()}");
			ArgumentSyntax argument = Argument(memberAccessExpression);
			InvocationExpressionSyntax invocationExpression = InvocationExpression(methodIdentifier, ArgumentList(SeparatedList(new[] { argument })));

			return AssignmentExpression(SyntaxKind.SimpleAssignmentExpression, IdentifierName(targetProperty.Name), invocationExpression)
				.WithLeadingCommentWhen(() => matchType != MatchType.Match, "todo: verify next line mapping!");
		}

		[SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
		private ExpressionSyntax GetDirectAssignment(ParameterSyntax sourceSyntax, IPropertySymbol sourceProperty, IPropertySymbol targetProperty, MatchType matchType)
		{
			MemberAccessExpressionSyntax memberAccessExpression = GetMemberAccess(sourceSyntax.Identifier, sourceProperty.Name);
			return AssignmentExpression(SyntaxKind.SimpleAssignmentExpression, IdentifierName(targetProperty.Name), memberAccessExpression)
				.WithLeadingCommentWhen(() => matchType != MatchType.Match, "todo: verify next line mapping!");
		}

		private ExpressionSyntax GetMissingMemberAssignment(ISymbol targetProperty, MatchType matchType)
		{
			return AssignmentExpression(SyntaxKind.SimpleAssignmentExpression, IdentifierName(targetProperty.Name), IdentifierName("NO_MATCHING_PROPERTY_FOUND"))
				.WithLeadingCommentWhen(() => matchType != MatchType.Match, "todo: fix next line mapping");
		}

		private ExpressionSyntax GetUnsupportedMappingTypeAssignment(ParameterSyntax sourceSyntax,
			IPropertySymbol sourceProperty,
			IPropertySymbol targetProperty,
			MatchType matchType)
		{
			MemberAccessExpressionSyntax memberAccessExpression = GetMemberAccess(sourceSyntax.Identifier, sourceProperty.Name);

			ExpressionSyntax methodIdentifier = IdentifierName("NotImplementedCollectionReduceMethod");
			ArgumentSyntax argument = Argument(memberAccessExpression);

			InvocationExpressionSyntax invocationExpression = InvocationExpression(methodIdentifier, ArgumentList(SeparatedList(new[] { argument })));

			return AssignmentExpression(SyntaxKind.SimpleAssignmentExpression, IdentifierName(targetProperty.Name), invocationExpression)
					.WithLeadingComment("todo: implement next line mapping manually!")
					.WithLeadingCommentWhen(() => matchType != MatchType.Match, "todo: fix next line mapping");
		}
	}
}