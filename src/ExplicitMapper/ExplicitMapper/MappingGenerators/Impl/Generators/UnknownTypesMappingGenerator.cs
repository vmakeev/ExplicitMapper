﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExplicitMapper.Extensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace ExplicitMapper.MappingGenerators.Impl.Generators
{
	internal class UnknownTypesMappingGenerator : IMappingGenerator
	{
		public bool IsActuallySupported { get; } = false;

		/// <inheritdoc />
		public Task<(BlockSyntax blockSyntax, IEnumerable<IReferencedMethodInfo> referencedMethods)> GenerateMappingMethodBody(ParameterSyntax sourceSyntax,
			TypeSyntax targetSyntax,
			SyntaxToken targetIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull,
			CancellationToken cancellationToken)
		{
			var statements = new LinkedList<StatementSyntax>();

			IdentifierNameSyntax body = IdentifierName("EXPLICIT_MAPPER_TYPE_SYSTEM_FAILURE")
				.WithLeadingComment("todo: ExplicitMapper can not determine some types of current method's signature");

			statements.AddLast(ExpressionStatement(body));

			BlockSyntax result = Block(statements);

			return Task.FromResult((result, Enumerable.Empty<IReferencedMethodInfo>()));
		}
	}
}