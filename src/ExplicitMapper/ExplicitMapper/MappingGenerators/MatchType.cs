﻿namespace ExplicitMapper.MappingGenerators
{
	internal enum MatchType
	{
		NotMatch,
		PossibleMatch,
		Match
	}
}