﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ExplicitMapper.MappingGenerators
{
	internal interface IMappingGenerator
	{
		bool IsActuallySupported { get; }

		Task<(BlockSyntax blockSyntax, IEnumerable<IReferencedMethodInfo> referencedMethods)> GenerateMappingMethodBody(
			ParameterSyntax sourceSyntax,
			TypeSyntax targetSyntax,
			SyntaxToken targetIdentifier,
			ITypeSymbol sourceType,
			ITypeSymbol targetType,
			bool allowParameterNull,
			CancellationToken cancellationToken);
	}
}