﻿using ExplicitMapper.Extensions;
using ExplicitMapper.MappingGenerators.Impl.Generators;
using Microsoft.CodeAnalysis;

namespace ExplicitMapper.MappingGenerators
{
	internal static class MappingGeneratorFactory
	{
		public static IMappingGenerator ConstructGenerator(ITypeSymbol sourceType, ITypeSymbol targetType)
		{
			if (sourceType == null || targetType == null)
			{
				return new UnknownTypesMappingGenerator();
			}

			if (targetType.IsEnum() && sourceType.IsEnum())
			{
				return new EnumMappingGenerator();
			}

			if (targetType.IsEnum() && !sourceType.IsEnum())
			{
				return new NonEnumToEnumMappingGenerator();
			}

			if (!targetType.IsEnum() && sourceType.IsEnum())
			{
				return new EnumToNonEnumMappingGenerator();
			}

			if (targetType.IsString() || targetType.IsValueType)
			{
				return new UnsupportedMappingGenerator();
			}

			if (sourceType.IsString() || sourceType.IsValueType)
			{
				return new UnsupportedMappingGenerator();
			}

			return new ClassMappingGenerator();
		}
	}
}