﻿using System.Collections.Immutable;
using System.Composition;
using ExplicitMapper.CodeFixes.Base;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;

namespace ExplicitMapper.CodeFixes
{
	[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(StaticExtensionMapFullCodeFixProvider))]
	[Shared]
	public class StaticExtensionMapFullCodeFixProvider : MapFullCodeFixProviderBase
	{
		protected override string CurrentDiagnosticId { get; } = DiagnosticId.StaticExtensionFullMap;

		public override ImmutableArray<string> FixableDiagnosticIds { get; } = ImmutableArray.Create(DiagnosticId.StaticExtensionFullMap);

		protected override bool AllowParameterNull { get; } = false;

		protected override string Title { get; } = "Generate mappings";
	}
}