﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExplicitMapper.Extensions;
using ExplicitMapper.MappingGenerators;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace ExplicitMapper.CodeFixes.Base
{
	/// <inheritdoc />
	public abstract class MapFullCodeFixProviderBase : CodeFixProvider
	{
		private static readonly IEqualityComparer<ITypeSymbol> _typesComparer = new TypeSymbolEqualityComparer();

		protected abstract bool AllowParameterNull { get; }

		protected abstract string Title { get; }

		protected abstract string CurrentDiagnosticId { get; }

		protected static Guid CodeFixId { get; } = Guid.NewGuid();

		public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
		{
			SyntaxNode root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

			Diagnostic diagnostic = context.Diagnostics.Single(item => item.Id == CurrentDiagnosticId);
			TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

			MethodDeclarationSyntax declaration = root.FindToken(diagnosticSpan.Start)
													.Parent
													.AncestorsAndSelf()
													.OfType<MethodDeclarationSyntax>()
													.Single(syntax => syntax.GetLocation().SourceSpan == diagnosticSpan);

			SyntaxNode declarationParent = declaration.Parent;
			string className = null;
			while (declarationParent != null)
			{
				if (declarationParent is ClassDeclarationSyntax classDeclaration)
				{
					className = classDeclaration.Identifier.Text;
					break;
				}

				declarationParent = declarationParent.Parent;
			}

			// No class name - no code fix
			if (string.IsNullOrEmpty(className))
			{
				return;
			}

			context.RegisterCodeFix(
				CodeAction.Create(
					title: Title,
					createChangedDocument: cancellationToken => ModifyExistingMethod(context.Document, className, declaration, AllowParameterNull, new HashSet<string>(), cancellationToken),
					equivalenceKey: CodeFixId.ToString()),
				diagnostic);
		}

		private ClassDeclarationSyntax FindClass(SyntaxNode root, string name, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			foreach (SyntaxNode childNode in root.ChildNodes())
			{
				if (childNode is ClassDeclarationSyntax classDeclaration && classDeclaration.Identifier.Text == name)
				{
					return classDeclaration;
				}

				ClassDeclarationSyntax found = FindClass(childNode, name, cancellationToken);

				if (found != null)
				{
					return found;
				}
			}

			return null;
		}

		private async Task<Document> ModifyExistingMethod(Document document,
			string className,
			MethodDeclarationSyntax methodDeclaration,
			bool allowParameterNull,
			ISet<string> processedDeclarations,
			CancellationToken cancellationToken)
		{
			string declarationSign = GetMethodDeclarationSign(methodDeclaration);
			if (processedDeclarations.Contains(declarationSign))
			{
				return document;
			}

			processedDeclarations.Add(declarationSign);

			ParameterSyntax sourceSyntax = methodDeclaration.ParameterList.Parameters.Single();
			TypeSyntax targetSyntax = methodDeclaration.ReturnType;

			TypeSyntax sourceTypeSyntax = sourceSyntax.Type;
			TypeSyntax targetTypeSyntax = targetSyntax;

			SemanticModel semanticModel = await document.GetSemanticModelAsync(cancellationToken).ConfigureAwait(false);

			SymbolInfo sourceTypeSymbolInfo = semanticModel.GetSymbolInfo(sourceTypeSyntax, cancellationToken);
			SymbolInfo targetTypeSymbolInfo = semanticModel.GetSymbolInfo(targetTypeSyntax, cancellationToken);

			var sourceTypeSymbol = (ITypeSymbol)sourceTypeSymbolInfo.Symbol;
			var targetTypeSymbol = (ITypeSymbol)targetTypeSymbolInfo.Symbol;

			BlockSyntax oldBodySyntax = methodDeclaration.ChildNodes().OfType<BlockSyntax>().Single();

			const string primaryTargetName = "target";
			const string secondaryTargetName = "destination";

			SyntaxToken targetIdentifier = sourceSyntax.Identifier.Text == primaryTargetName
				? SyntaxFactory.Identifier(secondaryTargetName)
				: SyntaxFactory.Identifier(primaryTargetName);

			IMappingGenerator generator = MappingGeneratorFactory.ConstructGenerator(sourceTypeSymbol, targetTypeSymbol);

			bool isDirectMethodModification = processedDeclarations.Count == 1;
			bool isBodySyntaxEmpty = !oldBodySyntax.Statements.Any();

			if (!generator.IsActuallySupported && !isBodySyntaxEmpty && !isDirectMethodModification)
			{
				return document;
			}

			(BlockSyntax newBodySyntax, IEnumerable<IReferencedMethodInfo> referencedMethods) = await generator.GenerateMappingMethodBody(
				sourceSyntax: sourceSyntax,
				targetSyntax: targetSyntax,
				targetIdentifier: targetIdentifier,
				sourceType: sourceTypeSymbol,
				targetType: targetTypeSymbol,
				allowParameterNull: allowParameterNull,
				cancellationToken: cancellationToken).ConfigureAwait(false);

			MethodDeclarationSyntax newMethodDeclaration = methodDeclaration.ReplaceNode(oldBodySyntax, newBodySyntax);

			SyntaxNode root = await document.GetSyntaxRootAsync(cancellationToken);
			SyntaxNode newRoot = root.ReplaceNode(methodDeclaration, newMethodDeclaration);

			Document modifiedDocument = document.WithSyntaxRoot(newRoot);

			foreach (IReferencedMethodInfo referencedMethod in referencedMethods)
			{
				SyntaxNode targetRoot = await modifiedDocument.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false);
				SemanticModel targetSemanticModel = await modifiedDocument.GetSemanticModelAsync(cancellationToken).ConfigureAwait(false);

				MethodDeclarationSyntax existingSyntax = FindMethodDeclarationSyntax(
					targetRoot,
					targetSemanticModel,
					referencedMethod,
					cancellationToken);

				if (existingSyntax != null && !referencedMethod.IsSpecialMapCollectionMethod())
				{
					modifiedDocument = await ModifyExistingMethod(modifiedDocument, className, existingSyntax, true, processedDeclarations, cancellationToken).ConfigureAwait(false);
				}
				else
				{
					modifiedDocument = await AddNewMethod(modifiedDocument, className, referencedMethod, processedDeclarations, cancellationToken).ConfigureAwait(false);
				}
			}

			return modifiedDocument;
		}

		private bool IsSatisfiedBy(MethodDeclarationSyntax methodDeclaration,
			SemanticModel semanticModel,
			IReferencedMethodInfo methodInfo,
			CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();

			if (methodDeclaration.Identifier.Text != methodInfo.Name)
			{
				return false;
			}

			// костыль :(
			if (methodInfo.IsSpecialMapCollectionMethod())
			{
				return true;
			}

			SymbolInfo methodReturnTypeSymbolInfo = semanticModel.GetSymbolInfo(methodDeclaration.ReturnType);
			var methodReturnTypeSymbol = (ITypeSymbol)methodReturnTypeSymbolInfo.Symbol;

			if (!_typesComparer.Equals(methodReturnTypeSymbol, methodInfo.ResultType))
			{
				return false;
			}

			if (methodInfo.ParameterTypes.Count != (methodDeclaration.ParameterList?.Parameters.Count ?? 0))
			{
				return false;
			}

			if (methodInfo.ParameterTypes.Any() && methodDeclaration.ParameterList != null)
			{
				if (methodInfo.ParameterTypes.Count != methodDeclaration.ParameterList.Parameters.Count)
				{
					return false;
				}

				for (int i = 0; i < methodInfo.ParameterTypes.Count; i++)
				{
					ITypeSymbol requiredParameter = methodInfo.ParameterTypes[i];

					ParameterSyntax parameterSyntax = methodDeclaration.ParameterList.Parameters[i];
					SymbolInfo methodParameterTypeSymbolInfo = semanticModel.GetSymbolInfo(parameterSyntax.Type);
					var methodParameterTypeSymbol = (ITypeSymbol)methodParameterTypeSymbolInfo.Symbol;

					if (!_typesComparer.Equals(methodParameterTypeSymbol, requiredParameter))
					{
						return false;
					}
				}
			}

			return true;
		}

		private MethodDeclarationSyntax FindMethodDeclarationSyntax(SyntaxNode syntax,
			SemanticModel semanticModel,
			IReferencedMethodInfo methodInfo,
			CancellationToken cancellationToken)
		{
			return syntax
					.DescendantNodes(node => true)
					.OfType<MethodDeclarationSyntax>()
					.FirstOrDefault(methodDeclaration => IsSatisfiedBy(methodDeclaration, semanticModel, methodInfo, cancellationToken));
		}

		private async Task<Document> AddNewMethod(Document document,
			string className,
			IReferencedMethodInfo methodInfo,
			ISet<string> processedDeclarations,
			CancellationToken cancellationToken)
		{
			if (methodInfo.Name == "MapCollection")
			{
				return await AddMapCollectionMethod(document, className, methodInfo, processedDeclarations, cancellationToken).ConfigureAwait(false);
			}

			SyntaxNode root = await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false);

			ClassDeclarationSyntax classToModify = FindClass(root, className, cancellationToken);

			var ns = classToModify.FindParent<NamespaceDeclarationSyntax>();
			IEnumerable<UsingDirectiveSyntax> documentUsings = root.ChildNodes().OfType<UsingDirectiveSyntax>();
			IEnumerable<UsingDirectiveSyntax> nsUsings = ns?.ChildNodes().OfType<UsingDirectiveSyntax>() ?? Enumerable.Empty<UsingDirectiveSyntax>();
			UsingDirectiveSyntax[] allUsings = documentUsings.Concat(nsUsings).ToArray();

			MethodDeclarationSyntax methodDeclaration = methodInfo.GetMethodDeclarationSyntax(allUsings, ns);

			ClassDeclarationSyntax modifiedClass = classToModify.AddMembers(methodDeclaration);

			root = root.ReplaceNode(classToModify, modifiedClass);

			document = document.WithSyntaxRoot(root);

			root = await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false);
			SemanticModel semanticModel = await document.GetSemanticModelAsync(cancellationToken).ConfigureAwait(false);

			MethodDeclarationSyntax added = FindMethodDeclarationSyntax(root, semanticModel, methodInfo, cancellationToken);

			if (methodInfo.ResultType.IsIEnumerable(true)) // not supported
			{
				return document;
			}
			else
			{
				return await ModifyExistingMethod(document, className, added, true, processedDeclarations, cancellationToken).ConfigureAwait(false);
			}
		}

		private async Task<Document> AddMapCollectionMethod(Document document,
			string className,
			IReferencedMethodInfo methodInfo,
			ISet<string> processedDeclarations,
			CancellationToken cancellationToken)
		{
			if (processedDeclarations.Contains(methodInfo.Name))
			{
				return document;
			}

			processedDeclarations.Add(methodInfo.Name);

			string methodText = @"private static List<TTarget> " +
								methodInfo.Name +
								@"<TSource, TTarget>(ICollection<TSource> source, Func<TSource, TTarget> map)
		{
			if (source == null)
			{
				return new List<TTarget>();
			}

			var target = new List<TTarget>(source.Count);

			foreach (TSource sourceItem in source)
			{
				TTarget targetItem = map(sourceItem);
				target.Add(targetItem);
			}

			return target;
		}";

			SyntaxTree methodDeclarationTree = SyntaxFactory.ParseSyntaxTree(methodText);

			var methodDeclaration = methodDeclarationTree.GetRoot(cancellationToken).ChildNodes().Single() as MethodDeclarationSyntax;

			SyntaxNode root = await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false);

			ClassDeclarationSyntax classToModify = FindClass(root, className, cancellationToken);
			ClassDeclarationSyntax modifiedClass = classToModify.AddMembers(methodDeclaration);

			root = root.ReplaceNode(classToModify, modifiedClass);

			document = document.WithSyntaxRoot(root);

			return document;
		}

		private static string GetMethodDeclarationSign(MethodDeclarationSyntax methodDeclaration)
		{
			string returnType = methodDeclaration.ReturnType.ToString();
			string methodName = methodDeclaration.Identifier.Text;

			IEnumerable<(string Type, string Name)> arguments = methodDeclaration.ParameterList?.Parameters.Select(parameter =>
																{
																	string parameterType = parameter.Type?.ToString();
																	string parameterName = parameter.Identifier.Text;
																	return (parameterType, parameterName);
																}) ??
																Enumerable.Empty<(string, string)>();

			string argumentsString = string.Join(", ", arguments.Select(argument => $"{argument.Type} {argument.Name}"));

			string declarationSign = $"{returnType} {methodName}({argumentsString})";
			return declarationSign;
		}
	}
}