using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExplicitMapper.MappingGenerators;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace ExplicitMapper.CodeFixes.Base
{
	public abstract class MapCodeFixProviderBase : CodeFixProvider
	{
		protected abstract bool AllowParameterNull { get; }

		protected abstract string Title { get; }

		protected abstract string CurrentDiagnosticId { get; }

		protected static Guid CodeFixId { get; } = Guid.NewGuid();

		public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
		{
			SyntaxNode root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

			Diagnostic diagnostic = context.Diagnostics.Single(item => item.Id == CurrentDiagnosticId);
			TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

			MethodDeclarationSyntax declaration = root.FindToken(diagnosticSpan.Start)
													.Parent
													.AncestorsAndSelf()
													.OfType<MethodDeclarationSyntax>()
													.Single(syntax => syntax.GetLocation().SourceSpan == diagnosticSpan);

			context.RegisterCodeFix(
				CodeAction.Create(
					title: Title,
					createChangedDocument: cancellationToken => GenerateClassMappping(context, declaration, cancellationToken),
					equivalenceKey: CodeFixId.ToString()),
				diagnostic);
		}

		private async Task<Document> GenerateClassMappping(CodeFixContext context, MethodDeclarationSyntax methodDeclaration, CancellationToken cancellationToken)
		{
			ParameterSyntax sourceSyntax = methodDeclaration.ParameterList.Parameters.Single();
			TypeSyntax targetSyntax = methodDeclaration.ReturnType;

			TypeSyntax sourceTypeSyntax = sourceSyntax.Type;
			TypeSyntax targetTypeSyntax = targetSyntax;

			SemanticModel semanticModel = await context.Document.GetSemanticModelAsync(cancellationToken).ConfigureAwait(false);

			SymbolInfo sourceTypeSymbolInfo = semanticModel.GetSymbolInfo(sourceTypeSyntax, cancellationToken);
			SymbolInfo targetTypeSymbolInfo = semanticModel.GetSymbolInfo(targetTypeSyntax, cancellationToken);

			var sourceTypeSymbol = (ITypeSymbol)sourceTypeSymbolInfo.Symbol;
			var targetTypeSymbol = (ITypeSymbol)targetTypeSymbolInfo.Symbol;

			SyntaxNode root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);
			BlockSyntax oldBodySyntax = methodDeclaration.ChildNodes().OfType<BlockSyntax>().Single();

			const string primaryTargetName = "target";
			const string secondaryTargetName = "destination";

			SyntaxToken targetIdentifier = sourceSyntax.Identifier.Text == primaryTargetName
				? SyntaxFactory.Identifier(secondaryTargetName)
				: SyntaxFactory.Identifier(primaryTargetName);

			IMappingGenerator generator = MappingGeneratorFactory.ConstructGenerator(sourceTypeSymbol, targetTypeSymbol);

			(BlockSyntax newBodySyntax, _) = await generator.GenerateMappingMethodBody(
				sourceSyntax: sourceSyntax,
				targetSyntax: targetSyntax,
				targetIdentifier: targetIdentifier,
				sourceType: sourceTypeSymbol,
				targetType: targetTypeSymbol,
				allowParameterNull: AllowParameterNull,
				cancellationToken: cancellationToken).ConfigureAwait(false);

			SyntaxNode newRoot = root.ReplaceNode(oldBodySyntax, newBodySyntax);
			return context.Document.WithSyntaxRoot(newRoot);
		}
	}
}