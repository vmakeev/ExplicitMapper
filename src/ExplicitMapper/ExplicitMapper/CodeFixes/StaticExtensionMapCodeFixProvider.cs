﻿using System.Collections.Immutable;
using System.Composition;
using ExplicitMapper.CodeFixes.Base;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;

namespace ExplicitMapper.CodeFixes
{
	[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(StaticExtensionMapCodeFixProvider))]
	[Shared]
	public class StaticExtensionMapCodeFixProvider : MapCodeFixProviderBase
	{
		public override ImmutableArray<string> FixableDiagnosticIds { get; } = ImmutableArray.Create(DiagnosticId.StaticExtensionMap);

		protected override bool AllowParameterNull { get; } = false;

		protected override string Title { get; } = "Generate mapping";

		protected override string CurrentDiagnosticId { get; } = DiagnosticId.StaticExtensionMap;
	}
}