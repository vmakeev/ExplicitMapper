﻿using System.Collections.Immutable;
using System.Composition;
using ExplicitMapper.CodeFixes.Base;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;

namespace ExplicitMapper.CodeFixes
{
	[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(StaticMapFullCodeFixProvider))]
	[Shared]
	public class StaticMapFullCodeFixProvider : MapFullCodeFixProviderBase
	{
		protected override string CurrentDiagnosticId { get; } = DiagnosticId.StaticFullMap;

		public override ImmutableArray<string> FixableDiagnosticIds { get; } = ImmutableArray.Create(DiagnosticId.StaticFullMap);

		protected override bool AllowParameterNull { get; } = true;

		protected override string Title { get; } = "Generate mappings";
	}
}