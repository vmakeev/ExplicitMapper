﻿using System.Collections.Immutable;
using System.Composition;
using ExplicitMapper.CodeFixes.Base;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;

namespace ExplicitMapper.CodeFixes
{
	[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(StaticMapCodeFixProvider))]
	[Shared]
	public class StaticMapCodeFixProvider : MapCodeFixProviderBase
	{
		public override ImmutableArray<string> FixableDiagnosticIds { get; } = ImmutableArray.Create(DiagnosticId.StaticMap);

		protected override bool AllowParameterNull { get; } = true;

		protected override string Title { get; } = "Generate mapping";

		protected override string CurrentDiagnosticId { get; } = DiagnosticId.StaticMap;
	}
}