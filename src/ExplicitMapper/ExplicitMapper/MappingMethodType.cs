namespace ExplicitMapper
{
	internal enum MappingMethodType
	{
		INVALID,
		NotMapping,
		DirectMap,
		StaticDirectMap,
		StaticExtensionMap
	}
}