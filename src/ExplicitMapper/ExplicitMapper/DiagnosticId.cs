namespace ExplicitMapper
{
	internal static class DiagnosticId
	{
		public static string Map { get; } = "Map";

		public static string StaticMap { get; } = "StaticMap";

		public static string StaticFullMap { get; } = "StaticFullMap";

		public static string StaticExtensionMap { get; } = "StaticExtensionMap";

		public static string StaticExtensionFullMap { get; } = "StaticExtensionFullMap";
	}
}