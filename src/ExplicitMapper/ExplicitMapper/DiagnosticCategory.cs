namespace ExplicitMapper
{
	internal static class DiagnosticCategory
	{
		private const string CommonCategory = "CodeGeneration";

		public static string StaticMap { get; } = CommonCategory;

		public static string StaticFullMap { get; } = CommonCategory;

		public static string DirectMap { get; } = CommonCategory;

		public static string StaticExtensionMap { get; } = CommonCategory;

		public static string StaticExtensionFullMap { get; } = CommonCategory;
	}
}