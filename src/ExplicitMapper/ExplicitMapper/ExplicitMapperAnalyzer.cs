using System;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using ExplicitMapper.Extensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;

namespace ExplicitMapper
{
	[DiagnosticAnalyzer(LanguageNames.CSharp)]
	public class ExplicitMapperAnalyzer : DiagnosticAnalyzer
	{
		public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
			ImmutableArray.Create(
				DiagnosticDescriptors.StaticMap,
				DiagnosticDescriptors.StaticFullMap,
				DiagnosticDescriptors.StaticExtensionMap,
				DiagnosticDescriptors.StaticExtensionFullMap);

		public override void Initialize(AnalysisContext context)
		{
			context.RegisterSyntaxNodeAction(AnalyzeMethodSignature, SyntaxKind.MethodDeclaration);
		}

		[SuppressMessage("ReSharper", "RedundantCaseLabel")]
		private static void AnalyzeMethodSignature(SyntaxNodeAnalysisContext context)
		{
			if (context.SemanticModel.GetDeclaredSymbol(context.Node) is IMethodSymbol methodSymbol)
			{
				MappingMethodType type = GetMappingMethodType(methodSymbol);

				switch (type)
				{
					case MappingMethodType.NotMapping:
						break;

					case MappingMethodType.DirectMap:
						// todo: implement
						//context.ReportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.DirectMap, context.Node.GetLocation()));
						break;

					case MappingMethodType.StaticDirectMap:
						context.ReportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.StaticMap, context.Node.GetLocation()));
						context.ReportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.StaticFullMap, context.Node.GetLocation()));
						break;

					case MappingMethodType.StaticExtensionMap:
						context.ReportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.StaticExtensionMap, context.Node.GetLocation()));
						context.ReportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.StaticExtensionFullMap, context.Node.GetLocation()));
						break;

					case MappingMethodType.INVALID:
					default:
						throw new ArgumentOutOfRangeException(nameof(MappingMethodType));
				}
			}
		}

		private static MappingMethodType GetMappingMethodType(IMethodSymbol methodSymbol)
		{
			if (!IsDirectMappingMethod(methodSymbol))
			{
				return MappingMethodType.NotMapping;
			}

			if (methodSymbol.IsExtensionMethod)
			{
				return MappingMethodType.StaticExtensionMap;
			}

			if (methodSymbol.IsStatic)
			{
				return MappingMethodType.StaticDirectMap;
			}

			return MappingMethodType.DirectMap;
		}

		private static bool IsDirectMappingMethod(IMethodSymbol methodSymbol)
		{
			if (methodSymbol.Parameters.Length != 1)
			{
				return false;
			}

			string methodName = methodSymbol.Name;
			ITypeSymbol targetType = methodSymbol.ReturnType;

			if (methodName != null && methodName.StartsWith("To"))
			{
				string targetTypeName = methodName.Substring(2);

				return targetType.Name == targetTypeName ||
						targetType.GetSemanticName() == targetTypeName ||
						targetType.GetUnderlyingName() == targetTypeName;
			}

			return false;
		}
	}
}