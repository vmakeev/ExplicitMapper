using Microsoft.CodeAnalysis;

namespace ExplicitMapper
{
	internal static class DiagnosticDescriptors
	{
		public static DiagnosticDescriptor StaticMap { get; } = new DiagnosticDescriptor(
			DiagnosticId.StaticMap,
			@"Automatic class mappings generator",
			@"it's possible to generate class mapping",
			DiagnosticCategory.StaticMap,
			DiagnosticSeverity.Info,
			isEnabledByDefault: true,
			description: @"Automatic class mappings generator");

		public static DiagnosticDescriptor StaticFullMap { get; } = new DiagnosticDescriptor(
			DiagnosticId.StaticFullMap,
			@"Automatic class mappings generator, includes dependent mappings",
			@"it's possible to generate class mapping",
			DiagnosticCategory.StaticFullMap,
			DiagnosticSeverity.Info,
			isEnabledByDefault: true,
			description: @"Automatic class mappings generator, includes dependent mappings");

		public static DiagnosticDescriptor DirectMap { get; } = new DiagnosticDescriptor(
			DiagnosticId.Map,
			@"Automatic class mappings generator",
			@"it's possible to generate class mapping",
			DiagnosticCategory.DirectMap,
			DiagnosticSeverity.Info,
			isEnabledByDefault: true,
			description: @"Automatic class mappings generator");

		public static DiagnosticDescriptor StaticExtensionMap { get; } = new DiagnosticDescriptor(
			DiagnosticId.StaticExtensionMap,
			@"Automatic class mappings generator",
			@"it's possible to generate class mapping",
			DiagnosticCategory.StaticExtensionMap,
			DiagnosticSeverity.Info,
			isEnabledByDefault: true,
			description: @"Automatic class mappings generator");

		public static DiagnosticDescriptor StaticExtensionFullMap { get; } = new DiagnosticDescriptor(
			DiagnosticId.StaticExtensionFullMap,
			@"Automatic class mappings generator",
			@"it's possible to generate class mapping",
			DiagnosticCategory.StaticExtensionFullMap,
			DiagnosticSeverity.Info,
			isEnabledByDefault: true,
			description: @"Automatic class mappings generator, includes dependent mappings");
	}
}