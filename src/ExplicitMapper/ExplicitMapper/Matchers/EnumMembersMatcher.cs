﻿using System.Collections.Generic;
using System.Linq;
using ExplicitMapper.Extensions;
using ExplicitMapper.MappingGenerators;
using ExplicitMapper.Matchers.Impl;
using Microsoft.CodeAnalysis;

namespace ExplicitMapper.Matchers
{
	internal static class EnumMembersMatcher
	{
		private static readonly INameMatcher _defaultMatcher = new SubWordsSubstringNameMatcher();

		public static IEnumerable<Match<IFieldSymbol>> MatchMembers(ITypeSymbol sourceTypeSymbol, ITypeSymbol targetTypeSymbol)
		{
			return MatchMembers(sourceTypeSymbol, targetTypeSymbol, _defaultMatcher);
		}

		public static IEnumerable<Match<IFieldSymbol>> MatchMembers(ITypeSymbol sourceTypeSymbol, ITypeSymbol targetTypeSymbol, INameMatcher matcher)
		{
			IEnumerable<IFieldSymbol> sourceMembers = sourceTypeSymbol.GetEnumMembers();
			IEnumerable<IFieldSymbol> targetMembers = targetTypeSymbol.GetEnumMembers().ToArray();

			if (!targetMembers.Any())
			{
				yield break;
			}

			MatchType GetMatchType(float match)
			{
				if (match > 0.99)
				{
					return MatchType.Match;
				}

				if (match < 0.3)
				{
					return MatchType.NotMatch;
				}

				return MatchType.PossibleMatch;
			}

			foreach (IFieldSymbol sourceMember in sourceMembers)
			{
				IEnumerable<(IFieldSymbol property, float match)> matches = targetMembers.Select(from => (from, matcher.Match(from.Name, sourceMember.Name)));

				(IFieldSymbol targetMember, float match) = matches.OrderByDescending(p => p.match).First();

				yield return new Match<IFieldSymbol>(sourceMember, targetMember, GetMatchType(match));
			}
		}
	}
}