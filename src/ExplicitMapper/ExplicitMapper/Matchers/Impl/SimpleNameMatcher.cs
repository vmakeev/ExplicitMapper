﻿using System;

namespace ExplicitMapper.Matchers.Impl
{
	/// <inheritdoc />
	internal class SimpleNameMatcher : INameMatcher
	{
		/// <inheritdoc />
		public virtual float Match(string from, string to)
		{
			if (string.Equals(from, to, StringComparison.OrdinalIgnoreCase))
			{
				return 1;
			}

			return 0;
		}
	}
}