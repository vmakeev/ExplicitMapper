﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExplicitMapper.Matchers.Impl
{
	internal class SubWordsSubstringNameMatcher : SubWordsNameMatcher
	{
		private const int MatchFirstSymbolScore = 5;
		private const int MatchSymbolScore = 3;
		private const float FullMatchBonus = 1.2f;

		/// <inheritdoc />
		protected override float MatchInternal(string from, string to, string[] splittedFrom, string[] splittedTo)
		{
			//float baseScore = base.MatchInternal(from, to, splittedFrom, splittedTo);

			string[] splittedFromLower = splittedFrom.Select(part => part.ToLowerInvariant()).ToArray();
			string[] splittedToLower = splittedTo.Select(part => part.ToLowerInvariant()).ToArray();

			float maxToScore = splittedFromLower.Select(part => GetMaxScore(splittedFromLower, part)).Sum();
			float[] matchedToScores = splittedToLower.Select(partTo => GetMaxScore(splittedFromLower, partTo)).ToArray();

			float maxFromScore = splittedToLower.Select(part => GetMaxScore(splittedToLower, part)).Sum();
			float[] matchedFromScores = splittedFromLower.Select(partFrom => GetMaxScore(splittedToLower, partFrom)).ToArray();

			float matchTo = matchedToScores.Sum() / maxToScore;
			float matchFrom = matchedFromScores.Sum() / maxFromScore;

			float result = (matchTo + matchFrom) / 2f;
			return result;
		}

		protected float GetMaxScore(IEnumerable<string> parts, string target)
		{
			float maxScore = 0;

			foreach (string part in parts)
			{
				int minLength = Math.Min(target.Length, part.Length);

				float localScore = 0;

				if (part == target)
				{
					localScore = (MatchFirstSymbolScore + (minLength - 1) * MatchSymbolScore) * FullMatchBonus;
				}
				else
				{
					for (int i = 0; i < minLength; i++)
					{
						if (part[i] != target[i])
						{
							break;
						}

						localScore += i == 0 ? MatchFirstSymbolScore : MatchSymbolScore;
					}
				}

				if (localScore > maxScore)
				{
					maxScore = localScore;
				}
			}

			return maxScore;
		}
	}
}