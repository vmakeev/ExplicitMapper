﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ExplicitMapper.Matchers.Impl
{
	internal class SubWordsNameMatcher : SimpleNameMatcher
	{
		private const float NotExactlyMatchPenaltyRatio = 0.9f;
		private static readonly Regex _regex = new Regex(@"(?<!^)([A-Z_][a-z]|(?<=[a-z])[A-Z_])", RegexOptions.Compiled);

		/// <inheritdoc />
		public sealed override float Match(string from, string to)
		{
			float baseMatch = base.Match(from, to);

			string[] splittedTo = Split(to).ToArray();
			string[] splittedFrom = Split(from).ToArray();

			if (!splittedTo.Any() || !splittedFrom.Any())
			{
				return baseMatch;
			}

			float currentMatch = MatchInternal(from, to, splittedFrom, splittedTo);

			float result = Math.Max(baseMatch, currentMatch);

			if (from != to)
			{
				result *= NotExactlyMatchPenaltyRatio;
			}

			return result;
		}

		protected virtual float MatchInternal(string from, string to, string[] splittedFrom, string[] splittedTo)
		{
			int matchedToSymbols = splittedTo
									.Where(partTo => splittedFrom.Any(partFrom => string.Equals(partTo, partFrom, StringComparison.OrdinalIgnoreCase)))
									.Select(partTo => partTo.Length)
									.Sum();

			int matchedFromSymbols = splittedFrom
									.Where(partFrom => splittedTo.Any(partTo => string.Equals(partFrom, partTo, StringComparison.OrdinalIgnoreCase)))
									.Select(partFrom => partFrom.Length)
									.Sum();

			float matchTo = (float)matchedToSymbols / splittedTo.Select(partTo => partTo.Length).Sum();
			float matchFrom = (float)matchedFromSymbols / splittedFrom.Select(partTo => partTo.Length).Sum();

			float result = (matchTo + matchFrom) / 2f;

			return result;
		}

		protected static IEnumerable<string> Split(string source)
		{
			return _regex.Replace(source, " $1")
						.Split(' ')
						.Select(part => part.Trim(' ', '_'))
						.Where(part => !string.IsNullOrEmpty(part));
		}
	}
}