﻿namespace ExplicitMapper.Matchers
{
	internal interface INameMatcher
	{
		float Match(string from, string to);
	}
}