﻿using ExplicitMapper.MappingGenerators;

namespace ExplicitMapper.Matchers
{
	internal class Match<T>
	{
		public T Source { get; }

		public T Target { get; }

		public MatchType MatchType { get; }

		public Match(T source, T target, MatchType matchType)
		{
			Source = source;
			Target = target;
			MatchType = matchType;
		}

		public void Deconstruct(out T source, out T target, out MatchType matchType)
		{
			source = Source;
			target = Target;
			matchType = MatchType;
		}
	}
}