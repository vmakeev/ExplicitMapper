﻿using System.Collections.Generic;
using System.Linq;
using ExplicitMapper.Extensions;
using ExplicitMapper.MappingGenerators;
using ExplicitMapper.Matchers.Impl;
using Microsoft.CodeAnalysis;

namespace ExplicitMapper.Matchers
{
	internal static class TypePropertiesMatcher
	{
		private static readonly INameMatcher _defaultMatcher = new SubWordsNameMatcher();

		public static IEnumerable<Match<IPropertySymbol>> MatchProperties(ITypeSymbol sourceTypeSymbol, ITypeSymbol targetTypeSymbol)
		{
			return MatchProperties(sourceTypeSymbol, targetTypeSymbol, _defaultMatcher);
		}

		public static IEnumerable<Match<IPropertySymbol>> MatchProperties(ITypeSymbol sourceTypeSymbol, ITypeSymbol targetTypeSymbol, INameMatcher matcher)
		{
			IEnumerable<IPropertySymbol> sourceProperties = sourceTypeSymbol.GetAllProperties().ToArray();
			IEnumerable<IPropertySymbol> targetProperties = targetTypeSymbol.GetAllProperties();

			if (!sourceProperties.Any())
			{
				yield break;
			}

			MatchType GetMatchType(float match)
			{
				if (match > 0.99)
				{
					return MatchType.Match;
				}

				if (match < 0.3)
				{
					return MatchType.NotMatch;
				}

				return MatchType.PossibleMatch;
			}

			foreach (IPropertySymbol targetProperty in targetProperties)
			{
				IEnumerable<(IPropertySymbol property, float match)> matches = sourceProperties.Select(from => (from, matcher.Match(from.Name, targetProperty.Name)));

				(IPropertySymbol sourceProperty, float match) = matches.OrderByDescending(p => p.match).First();

				yield return new Match<IPropertySymbol>(sourceProperty, targetProperty, GetMatchType(match));
			}
		}
	}
}