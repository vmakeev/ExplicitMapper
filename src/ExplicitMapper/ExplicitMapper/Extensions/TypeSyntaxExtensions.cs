using System.Collections.Generic;
using System.Linq;
using ExplicitMapper.MappingGenerators;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ExplicitMapper.Extensions
{
	internal static class TypeSyntaxExtensions
	{
		public static MethodDeclarationSyntax GetMethodDeclarationSyntax(this IReferencedMethodInfo methodInfo, ICollection<UsingDirectiveSyntax> usings, NamespaceDeclarationSyntax ns)
		{
			TypeSyntax returnType = GetTypeSyntax(methodInfo.ResultType, usings, ns).WithLeadingTrivia(SyntaxFactory.Space);
			SyntaxToken identifier = SyntaxFactory.Identifier(methodInfo.Name).WithLeadingTrivia(SyntaxFactory.Space);

			var parameters = new List<ParameterSyntax>();

			string GetParameterName(ITypeSymbol parameterType)
			{
				string name = parameterType.GetSemanticName();

				if (parameters.Any(p => p.Identifier.Text == name))
				{
					int index = 1;
					string numberedName = $"{name}{index}";

					// ReSharper disable once AccessToModifiedClosure
					while (parameters.Any(parameter => parameter.Identifier.Text == numberedName))
					{
						numberedName = $"{name}{++index}";
					}

					return numberedName;
				}

				return name;
			}

			foreach (ITypeSymbol parameterType in methodInfo.ParameterTypes)
			{
				string parameterName = parameters.Any() ? GetParameterName(parameterType) : "source";

				ParameterSyntax parameter = SyntaxFactory.Parameter(SyntaxFactory.Identifier(parameterName))
														.WithType(GetTypeSyntax(parameterType, usings, ns)
															.WithTrailingTrivia(SyntaxFactory.Space));

				parameters.Add(parameter);
			}

			MethodDeclarationSyntax methodDeclaration = SyntaxFactory.MethodDeclaration(returnType, identifier)
																	.AddModifiers(SyntaxFactory.Token(SyntaxKind.PrivateKeyword).WithTrailingTrivia(SyntaxFactory.Space), SyntaxFactory.Token(SyntaxKind.StaticKeyword))
																	.AddParameterListParameters(parameters.ToArray())
																	.AddBodyStatements();

			return methodDeclaration;
		}

		private static TypeSyntax GetTypeSyntax(ITypeSymbol typeSymbol, ICollection<UsingDirectiveSyntax> usings, NamespaceDeclarationSyntax ns)
		{
			if (typeSymbol == null)
			{
				SyntaxFactory.IdentifierName("UNKNOWN_TYPE");
			}

			if (typeSymbol.IsNullableStruct())
			{
				ITypeSymbol underlyingType = typeSymbol.GetUnderlyingType();
				TypeSyntax underlyingSyntax = GetTypeSyntax(underlyingType, usings, ns);
				return SyntaxFactory.NullableType(underlyingSyntax);
			}

			CompiledName compiledName = typeSymbol.GetCompiledName(usings, ns);

			TypeSyntax result = SyntaxFactory.ParseTypeName(compiledName);
			return result;
		}
	}
}