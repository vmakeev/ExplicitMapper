﻿using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace ExplicitMapper.Extensions
{
	internal static class SyntaxNodeExtensions
	{
		public static T WithLeadingCommentWhen<T>(this T node, Func<bool> condition, string comment) where T : SyntaxNode
		{
			return condition()
				? node.WithLeadingComment(comment)
				: node;
		}

		public static T WithLeadingComment<T>(this T node, string comment) where T : SyntaxNode
		{
			return node.WithLeadingTrivia(SyntaxTriviaList.Create(SyntaxFactory.SyntaxTrivia(SyntaxKind.SingleLineCommentTrivia, $"// {comment}\r\n")));
		}

		public static T FindParent<T>(this SyntaxNode node) where T : SyntaxNode
		{
			switch (node.Parent)
			{
				case null:
					return null;

				case T target:
					return target;

				default:
					return FindParent<T>(node.Parent);
			}
		}
	}
}