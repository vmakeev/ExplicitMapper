using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace ExplicitMapper.Extensions
{
	internal class TypeSymbolEqualityComparer : IEqualityComparer<ITypeSymbol>
	{
		/// <inheritdoc />
		public bool Equals(ITypeSymbol x, ITypeSymbol y)
		{
			if (x == null || y == null)
			{
				return x != null || y != null;
			}

			if (x.IsNullableStruct() != y.IsNullableStruct())
			{
				return false;
			}

			x = x.UnwrapIfNullable();
			y = y.UnwrapIfNullable();

			return x.MetadataName == y.MetadataName &&
					x.ContainingAssembly.MetadataName == y.ContainingAssembly.MetadataName &&
					x.ContainingModule?.MetadataName == y.ContainingModule?.MetadataName &&
					x.ContainingNamespace?.MetadataName == y.ContainingNamespace?.MetadataName;
		}

		/// <inheritdoc />
		public int GetHashCode(ITypeSymbol obj)
		{
			return obj?.GetHashCode() ?? 0;
		}
	}
}