using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ExplicitMapper.Extensions
{
	internal static class TypeSymbolExtensions
	{
		private static readonly TypeSymbolEqualityComparer _comparer = new TypeSymbolEqualityComparer();

		public static IEnumerable<IFieldSymbol> GetEnumMembers(this ITypeSymbol symbol)
		{
			if (!symbol.IsEnum())
			{
				return Enumerable.Empty<IFieldSymbol>();
			}

			return symbol.UnwrapIfNullable().GetMembers().OfType<IFieldSymbol>();
		}

		public static ITypeSymbol UnwrapIfNullable(this ITypeSymbol symbol)
		{
			if (symbol.MetadataName == "Nullable`1")
			{
				return symbol.GetUnderlyingType();
			}

			return symbol;
		}

		[SuppressMessage("ReSharper", "RedundantCaseLabel")]
		public static IEnumerable<IPropertySymbol> GetAllProperties(this ITypeSymbol symbol)
		{
			switch (symbol.TypeKind)
			{
				case TypeKind.Interface:
					return GetAllInterfaceProperties(symbol);

				case TypeKind.Class:
				case TypeKind.Struct:
					return GetAllClassProperties(symbol);

				case TypeKind.Unknown:
				case TypeKind.Array:
				case TypeKind.Delegate:
				case TypeKind.Dynamic:
				case TypeKind.Error:
				case TypeKind.Module:
				case TypeKind.Pointer:
				case TypeKind.TypeParameter:
				case TypeKind.Submission:
				case TypeKind.Enum:
				default:
					return Enumerable.Empty<IPropertySymbol>();
			}
		}

		public static bool CanBeDirectlyMappedTo(this ITypeSymbol from, ITypeSymbol to)
		{
			if (from.ToString() == to.ToString() && from.ContainingAssembly.MetadataName == to.ContainingAssembly.MetadataName)
			{
				return true;
			}

			if (to.IsNullableStruct())
			{
				ITypeSymbol underlyingTo = to.GetUnderlyingType();
				return from.CanBeDirectlyMappedTo(underlyingTo);
			}

			return false;
		}

		public static bool IsICollection(this ITypeSymbol symbol)
		{
			const string iCollectionMetadateName = "ICollection`1";
			return symbol.MetadataName == iCollectionMetadateName || symbol.AllInterfaces.Any(p => p.MetadataName == iCollectionMetadateName);
		}

		public static bool IsIEnumerable(this ITypeSymbol symbol, bool ignoreStrings)
		{
			if (ignoreStrings && symbol.IsString())
			{
				return false;
			}

			const string iCollectionMetadateName = "IEnumerable`1";
			return symbol.MetadataName == iCollectionMetadateName || symbol.AllInterfaces.Any(p => p.MetadataName == iCollectionMetadateName);
		}

		public static string GetSemanticName(this ITypeSymbol symbol)
		{
			switch (symbol)
			{
				case INamedTypeSymbol nullableSymbol when nullableSymbol.MetadataName == "Nullable`1":
					string underlyingNullableType = nullableSymbol.TypeArguments.Single().GetSemanticName();
					return $"Nullable{underlyingNullableType}";

				case INamedTypeSymbol namedTypeSymbol when (namedTypeSymbol.IsGenericType && namedTypeSymbol.TypeArguments.Any()):
					IEnumerable<string> genericSymbols = namedTypeSymbol.TypeArguments.Select(typeSymbol => typeSymbol.GetSemanticName());
					string genericSymbolsString = string.Join("_And_", genericSymbols);
					return $"{symbol.Name}Of{genericSymbolsString}";
			}

			return symbol.Name;
		}

		public static CompiledName GetCompiledName(this ITypeSymbol symbol, ICollection<UsingDirectiveSyntax> usings, NamespaceDeclarationSyntax ns)
		{
			switch (symbol)
			{
				case INamedTypeSymbol nullableSymbol when nullableSymbol.MetadataName == "Nullable`1":
					string underlyingNullableType = nullableSymbol.TypeArguments.Single().GetCompiledName(usings, ns);
					return new CompiledName($"System.Nullable<{underlyingNullableType}>");

				case INamedTypeSymbol namedTypeSymbol when (namedTypeSymbol.IsGenericType && namedTypeSymbol.TypeArguments.Any()):
					IEnumerable<CompiledName> genericSymbols = namedTypeSymbol.TypeArguments.Select(typeSymbol => typeSymbol.GetCompiledName(usings, ns));
					string genericSymbolsString = string.Join(", ", genericSymbols);

					INamespaceSymbol genericClassNamespace = symbol.ContainingNamespace;
					CompiledName genericClassName = GetCompiledName(symbol.Name, genericClassNamespace, usings, ns);

					return new CompiledName(genericClassName.Prefix, $"{genericClassName.Name}<{genericSymbolsString}>");
			}

			INamespaceSymbol symbolContainingNamespace = symbol.ContainingNamespace;
			return GetCompiledName(symbol.Name, symbolContainingNamespace, usings, ns);
		}

		public static CompiledName GetCompiledName(this string symbolName, INamespaceSymbol symbolNamespace, ICollection<UsingDirectiveSyntax> usings, NamespaceDeclarationSyntax ns)
		{
			string classNamespace = ns?.Name.ToString();

			string symbolNs = symbolNamespace.ToDisplayString();
			if (!string.IsNullOrEmpty(classNamespace) && classNamespace == symbolNs)
			{
				return new CompiledName(symbolName);
			}

			// direct alias
			string fullName = $"{symbolNs}.{symbolName}";
			foreach (UsingDirectiveSyntax usingDirective in usings)
			{
				if (usingDirective.Name.ToString() == fullName && usingDirective.Alias != null)
				{
					return new CompiledName(usingDirective.Alias.Name.Identifier.Text);
				}
			}

			// imported namespace
			foreach (UsingDirectiveSyntax usingDirective in usings)
			{
				if (usingDirective.Name.ToString() == symbolNs)
				{
					// aliased namespace
					if (usingDirective.Alias != null)
					{
						return new CompiledName(usingDirective.Alias.Name.Identifier.Text, symbolName);
					}

					return new CompiledName(symbolName);
				}
			}

			// partial namespace
			if (!string.IsNullOrEmpty(classNamespace) && symbolNs.StartsWith(classNamespace))
			{
				string targetNsPart = symbolNs.Substring(classNamespace.Length).TrimStart('.');
				return new CompiledName(targetNsPart, symbolName);
			}

			return new CompiledName(symbolNs, symbolName);
		}

		public static string GetUnderlyingSemanticName(this ITypeSymbol symbol)
		{
			switch (symbol)
			{
				case INamedTypeSymbol namedTypeSymbol when (namedTypeSymbol.IsGenericType && namedTypeSymbol.TypeArguments.Length == 1):
					return namedTypeSymbol.TypeArguments.Single().GetSemanticName();

				case INamedTypeSymbol namedTypeSymbol when (namedTypeSymbol.IsGenericType && namedTypeSymbol.TypeArguments.Length > 1):
					IEnumerable<string> semanticNames = namedTypeSymbol.TypeArguments.Select(GetSemanticName);
					return string.Join("_And_", semanticNames);
			}

			return symbol.Name;
		}

		public static string GetUnderlyingName(this ITypeSymbol symbol)
		{
			switch (symbol)
			{
				case INamedTypeSymbol namedTypeSymbol when (namedTypeSymbol.IsGenericType && namedTypeSymbol.TypeArguments.Length == 1):
					return namedTypeSymbol.TypeArguments.Single().Name;

				case INamedTypeSymbol namedTypeSymbol when (namedTypeSymbol.IsGenericType && namedTypeSymbol.TypeArguments.Length > 1):
					IEnumerable<string> names = namedTypeSymbol.TypeArguments.Select(p => p.Name);
					return string.Join("_And_", names);
			}

			return symbol.Name;
		}

		public static ITypeSymbol GetUnderlyingType(this ITypeSymbol symbol)
		{
			switch (symbol)
			{
				case INamedTypeSymbol namedTypeSymbol when (namedTypeSymbol.IsGenericType && namedTypeSymbol.TypeArguments.Length == 1):
					return namedTypeSymbol.TypeArguments.Single();
			}

			throw new ArgumentException("Can not get underlying type", nameof(symbol));
		}

		public static ITypeSymbol GetUnderlyingTypeIfIEnumerable(this ITypeSymbol symbol)
		{
			if (symbol.IsIEnumerable(true))
			{
				return symbol.GetUnderlyingType();
			}

			return symbol;
		}

		public static bool CanBeNull(this ITypeSymbol symbol)
		{
			return symbol.IsReferenceType || symbol.IsNullableStruct();
		}

		public static bool IsNullableStruct(this ITypeSymbol symbol)
		{
			return symbol.MetadataName == "Nullable`1";
		}

		public static bool IsAssignableFrom(this ITypeSymbol self, ITypeSymbol other)
		{
			if (self == null || other == null)
			{
				return false;
			}

			if (self.ToString() == other.ToString() && self.ContainingAssembly.MetadataName == other.ContainingAssembly.MetadataName)
			{
				return true;
			}

			return self.IsAssignableFrom(other.BaseType) || other.Interfaces.Any(self.IsAssignableFrom);
		}

		public static bool IsEnum(this ITypeSymbol symbol)
		{
			if (symbol.BaseType?.MetadataName == "Enum")
			{
				return true;
			}

			if (symbol.MetadataName == "Nullable`1" && symbol.GetUnderlyingType().IsEnum())
			{
				return true;
			}

			return false;
		}

		public static bool IsString(this ITypeSymbol symbol)
		{
			return symbol.MetadataName == "String";
		}

		public static bool IsEqualsTo(this ITypeSymbol a, ITypeSymbol b)
		{
			return _comparer.Equals(a, b);
		}

		internal static IEnumerable<IPropertySymbol> GetAllClassProperties(ITypeSymbol symbol)
		{
			if (symbol.BaseType != null)
			{
				foreach (IPropertySymbol basePropertySymbol in GetAllProperties(symbol.BaseType))
				{
					yield return basePropertySymbol;
				}
			}

			foreach (IPropertySymbol propertySymbol in symbol.GetMembers().OfType<IPropertySymbol>())
			{
				yield return propertySymbol;
			}
		}

		internal static IEnumerable<IPropertySymbol> GetAllInterfaceProperties(ITypeSymbol symbol)
		{
			IEnumerable<ITypeSymbol> allTypes = new[] { symbol }.Concat(symbol.AllInterfaces).Distinct(new TypeSymbolEqualityComparer());
			return allTypes.SelectMany(typeSymbol => typeSymbol.GetMembers().OfType<IPropertySymbol>());
		}
	}
}