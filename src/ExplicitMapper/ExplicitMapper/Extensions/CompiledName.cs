using System;

namespace ExplicitMapper.Extensions
{
	public struct CompiledName
	{
		public CompiledName(string name)
			: this()
		{
			Name = name;
		}

		public CompiledName(string prefix, string name)
			: this()
		{
			Prefix = prefix;
			Name = name;
		}

		public string Prefix { get; }

		public string Name { get; }

		public static implicit operator String(CompiledName compiledName)
		{
			return compiledName.ToString();
		}

		/// <summary>Returns the fully qualified type name of this instance.</summary>
		/// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
		public override string ToString()
		{
			if (string.IsNullOrEmpty(Prefix))
			{
				return Name;
			}

			return $"{Prefix}.{Name}";
		}
	}
}